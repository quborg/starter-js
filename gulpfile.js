'use strict';

let gulp        = require('gulp')
  , nodemon     = require('gulp-nodemon')
  , gutil       = require('gulp-util')
  , browserSync = require('browser-sync')
  , browserify  = require('browserify')
  , source      = require('vinyl-source-stream')
;

gulp
  .task('serve', function(){
    nodemon({
      script: 'App.js',
      ext: 'js',
      watch: ['backend']
    })
    .on('restart', function(){ console.log('Restarting Node Server App.js') })
  })

  .task('browserify', function() {
    gutil.log('Compiling JS....')
    browserify('frontend/main.js')
      .transform('babelify', { 
        presets: ["es2015","stage-1","stage-0","react"],
        plugins: ['transform-runtime','transform-decorators-legacy']
      })
      .bundle()
      .on('error', function (err) {
          gutil.log(err.message)
          browserSync.notify("Browserify Error! :", err)
          this.emit("end")
        })
      .pipe(source('main.js'))
      .pipe(gulp.dest('public/js'))
      .pipe(browserSync.stream({once: true})
    )
  })

  .task('sync', function() {
    browserSync.init({
      proxy: { target: 'localhost:3000' }
     ,files: ['public']
     ,port: 8000
    })
  })

gulp.task('default', ['browserify','serve','sync'], function() {
  gulp.watch('frontend/**/*.*', ['browserify'], browserSync.reload() )
  gulp.watch('public/styles/**/*.*', function() { browserSync.reload() } )
})
