var Constants = require('../constants/Constants'),
    Dispatcher = require('../dispatchers/Dispatcher')

module.exports = {
  setLanguage: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.LANGUAGE,
      item: item
    })
  }
  ,setStickyPosts: function(posts) {
    Dispatcher.handleViewAction({
      actionType: Constants.STICKY_POSTS,
      posts: posts
    })
  }
  ,setPosts: function(posts) {
    Dispatcher.handleViewAction({
      actionType: Constants.BLOG_POSTS,
      posts: posts
    })
  }
  ,setComments: function(comments) {
    Dispatcher.handleViewAction({
      actionType: Constants.COMMENTS,
      comments: comments
    })
  }
  ,setUsers: function(users) {
    Dispatcher.handleViewAction({
      actionType: Constants.USERS,
      users: users
    })
  }
  ,setUser: function(user) {
    Dispatcher.handleViewAction({
      actionType: Constants.USER,
      user: user
    })
  }
}
