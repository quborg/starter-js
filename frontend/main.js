'use strict'

import render from 'react-dom';
var React     = require('react')
  , ReactDOM  = require('react-dom')
  , Routes    = require('./site/Routes')

// window.$      = require('jquery')

import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

ReactDOM.render(Routes, document.getElementById('main'))
