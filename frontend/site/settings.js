'use strict'

module.exports = {

  mainMenu: [
    { name: "sales",    title: "المبيعات", to: "/sales"     },
    { name: "products", title: "السلع",    to: "/products"  },
    { name: "invoices", title: "الفواتير", to: "/invoices"  }
  ],

  Views: {
    Sales: {
      thead: [
        { name: "time", title: "التوقيت" },
        { name: "unities",   title: "عدد الوحدات" },
        { name: "total",     title: "المجموع" }
      ],
      addModal: {
        title: "تسجيل تذكرة البيع",
      },
      removeModal: {
        title: "المرجو التأكيد : هل تريد إزالة تسجيل هذا البيع ?",
      },
      editModal: {
        title: "تحرير المبيع المسجل",
      }
    },
    Products: {
      thead: [
        { name: "title",          title: "الاسم"                      },
        { name: "sale_price",     title: "ثمن البيع"                  },
        { name: "purchase_price", title: "ثمن الشراء",  hidden: true  },
        { name: "quantity",       title: "الكمية"                     },
        { name: "category",       title: "الصنف"                      }
      ],
      fields: [
        { 
          name: "title",
          title: "الاسم",
          type: "text"
        },
        { 
          name: "sale_price",
          title: "ثمن البيع",
          type: "number",
          min: 0,
          step: 0.01
        },
        { 
          name: "purchase_price",
          title: "ثمن الشراء",
          type: "number",
          min: 0,
          step: 0.01,
          hidden: true
        },
        { 
          name: "quantity",
          title: "الكمية",
          type: "number",
          min: 0
        }
      ],
      addModal: {
        title: "إظافة منتوج جديد",
      },
      removeModal: {
        title: "المرجو التأكيد : هل تريد إزالة المنتوج ?",
      },
      editModal: {
        title: "تحرير الخصائص",
      }
    }
  }

}
