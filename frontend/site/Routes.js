// 'use strict'

import React from 'react'
import Template from '../components/templates/Template'
import Dashboard from '../components/dashboard/Dashboard'

import { Router, Route, IndexRoute, browserHistory } from 'react-router'

    // import Sales from '../components/sales/Sales',
    // import Products from '../components/products/Products',
    // import Invoices from '../components/invoices/Invoices',

module.exports = (
  <Router history={browserHistory}>
    <Route path="/" component={Template}>
      <IndexRoute component={Dashboard}/>
    </Route>
  </Router>
)
      // <Route path="/sales" component={Sales}/>
      // <Route path="/products" component={Products}/>
      // <Route path="/invoices" component={Invoices}/>
