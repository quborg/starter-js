'use strict'

var _ = require('lodash'),
    React = require('react'),
    Assign = require('object-assign'),
    Mixins = require('../../mixins/Mixins'),
    BtnGly = require('../../templates/BtnGly'),
    Service = require('../../services/Categories')
import { Input, Button, Modal } from 'react-bootstrap'

Service.getCategories()

window.ajaxFlag = 0

function getCategories() {
  let Cats = Service.get_categories(),
      Vals = []
  Cats.map( (c,i) => { Vals["cat-"+c.id] = c.tag } )
  return {
    categories: Cats,
    values: Vals
  }
}

module.exports = React.createClass({
  mixins: [Mixins(getCategories)],
  getInitialState: function() {
    return {
      show: this.props.show,
      newItem: 0,
      toSave: [],
      toUpdate: [],
      toRemove: []
    }
  },
  initialization: function() {
    Service.getCategories()
    this.setState({
      newItem: 0,
      toSave: [],
      toUpdate: [],
      toRemove: []
    })
  },
  add: function() {
    let cats = this.state.categories,
        i = ++this.state.newItem
    this.setState({newItem:i})
    cats.unshift({ id:'new-'+i, tag:'', new:1 })
    this.setState({ categories: cats })
  },
  remove: function(id, cid, k, isNew, e) {
    let Cats = Assign([],this.state.categories),
        toSave = Assign([],this.state.toSave),
        toRemove = Assign([],this.state.toRemove),
        toUpdate = Assign([],this.state.toUpdate)
    if(isNew)
      _.remove(toSave, (u)=>{return u.id==id} )
    else {
      _.remove(toUpdate, (u)=>{return u.id==id} )
      toRemove.push(cid)
    }
    Cats.splice(k,1)
    this.setState({
      categories: Cats,
      toRemove: _.uniq(toRemove),
      toSave: _.uniq(toSave,(c)=>{return c.id}),
      toUpdate: _.uniq(toUpdate,(c)=>{return c.id})
    })
  },
  save: function() {
    let s = this.state,
        self = this
    if(s.toRemove.length) { ++window.ajaxFlag; Service.removeCategories(s.toRemove) }
    if(s.toSave.length)   { ++window.ajaxFlag; Service.saveCategories(s.toSave) }
    if(s.toUpdate.length) { ++window.ajaxFlag; Service.updateCategories(s.toUpdate) }
    let flag = setInterval(function(){
      if(!window.ajaxFlag) {
        self.initialization()
        clearInterval(flag)
        self.props.close()
      }
    }, 100)
  },
  handleChange: function(id, cid, isNew, e) {
    let Value = e.target.value,
        Values = Assign({},this.state.values),
        toSave = Assign([], this.state.toSave),
        toUpdate = Assign([], this.state.toUpdate)
    Values[id] = Value
    isNew ? toSave.unshift({id:id,tag:Value}) : toUpdate.unshift({id:cid,tag:Value})
    this.setState({
      values: Values,
      toSave: _.uniq(toSave,(c)=>{return c.id}),
      toUpdate: _.uniq(toUpdate,(c)=>{return c.id})
    })
  },
  render: function() {
    let add = (<BtnGly Class="sp-b-15" Style="primary" Click={this.add} Gly="download-alt" aftG=" إظافة صنف جديد"/>)
    return (
      <Modal id={this.props.id} show={this.props.show} onHide={this.props.close}>
        <Modal.Header>
          <Modal.Title>تعديل التصانيف</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            {add}
            {this.state.categories.map((m,k)=>{
              let id = "cat-"+m.id,
                  isNew = !!m.new ? !0 : !1
              return (
                <Input 
                  key={k} 
                  type="text" 
                  groupClassName={id} 
                  value={this.state.values[id]} 
                  onChange={this.handleChange.bind(null, id, m.id, isNew)} 
                  buttonAfter={<BtnGly Style="danger" Click={this.remove.bind(null, id, m.id, k, isNew)} Gly="remove"/>}/>
              )
            })}
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.close}>إلغاء</Button>
          <Button onClick={this.save} bsStyle="primary">حفظ</Button>
        </Modal.Footer>
      </Modal>
    )
  }
})
