'use strict'

var React = require('react'),
    ToolBar = require('./ToolBar'),
    Serialize = require('form-serialize'),
    Site = require('../../site/settings'),
    Mixins = require('../../mixins/Mixins'),
    Modal = require('../../templates/Modal'),
    Table = require('../../templates/Table'),
    Helper = require('../../helpers/Helper'),
    Services = require('../../services/Products'),
    ProductFields = require('./ProductFields')
import { Grid, Row, Button } from 'react-bootstrap'

function getProducts() {
  return { 
    products: Services.get_products()
  }
}

Services.getProducts()

module.exports = React.createClass({
  mixins: [Mixins(getProducts)],
  getInitialState: function() { 
    return { 
      removeModal: false,
      editModal: false,
      product: []
    }
  },
  openEdit: function(p) { this.setState({editModal: true, product: p}) },
  closeEdit: function() { this.setState({editModal: false}) },
  edit: function() { 
    if(Helper.ValidateProduct('edit-product')) {
      let form = document.querySelector('#edit-product form')
      Services.updateProduct(this.state.product.id,Serialize(form))
      this.closeEdit()
    }
  },
  openRemove: function(p) { this.setState({removeModal: true, product: p}) },
  closeRemove: function() { this.setState({removeModal: false}) },
  remove: function() {
    Services.removeProduct(this.state.product.id)
    this.closeRemove()
  },
  render: function() {
    let columns = Site.Views.Products.thead,
        classes = "table-bordered table-condensed table-hover",
        titleR = Site.Views.Products.removeModal.title,
        submitR = (<Button onClick={this.remove} bsStyle="danger">نعم</Button>),
        cancelR = (<Button onClick={this.closeRemove} bsStyle="primary">لا</Button>),
        titleE = Site.Views.Products.editModal.title,
        submitE = (<Button onClick={this.edit} bsStyle="info">حفظ</Button>),
        cancelE = (<Button onClick={this.closeEdit}>لا</Button>),
        bodyR = this.state.product.category+' : '+this.state.product.title,
        bodyE = (<ProductFields data={this.state.product}/>)
    return (
      <Grid fluid>
        <Row className="show-grid sp-b-10" fluid><ToolBar/></Row>
        <Row className="show-grid" fluid>
          {<Table className={classes} thead={columns} tbody={this.state.products} edit={this.openEdit} remove={this.openRemove} />}
        </Row>
        <Modal show={this.state.removeModal} onHide={this.closeRemove} title={titleR} submit={submitR} cancel={cancelR} body={bodyR}/>
        <Modal id="edit-product" show={this.state.editModal} onHide={this.closeEdit} title={titleE} submit={submitE} cancel={cancelE} body={bodyE}/>
      </Grid>
    )
  }
})
