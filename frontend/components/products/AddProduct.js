'use strict'

var React = require('react'),
    Serialize = require('form-serialize'),
    Site = require('../../site/settings'),
    Modal = require('../../templates/Modal'),
    Helper = require('../../helpers/Helper'),
    BtnGly = require('../../templates/BtnGly'),
    Services = require('../../services/Products'),
    ProductFields = require('./ProductFields')
import { Button } from 'react-bootstrap'

module.exports = React.createClass({
  getInitialState: function() { return { showModal: false, }},
  open: function() { this.setState({showModal:true}) },
  close:function() { this.setState({showModal:false}) },
  save: function() {
    if(Helper.ValidateProduct('add-product')) {
      let form = document.querySelector('#add-product form')
      Services.saveProduct(Serialize(form))
      this.setState({ showModal: false })
    }
  },
  render: function() {
    let title = Site.Views.Products.addModal.title,
        save = (<Button onClick={this.save} bsStyle="primary">حفظ</Button>),
        cancel = (<Button onClick={this.close}>إلغاء</Button>),
        body = (<ProductFields/>)
    return (
      <div>
        <BtnGly Style="primary" Click={this.open} Gly="download-alt" aftG=" جديد"/>
        <Modal id="add-product" show={this.state.showModal} onHide={this.close} title={title} submit={save} cancel={cancel} body={body}/>
      </div>
    )
  }
})
