'use strict'

var $ = require('jquery'),
    React = require('react'),
    Assign = require('object-assign'),
    Mixins = require('../../mixins/Mixins'),
    Services = require('../../services/Products'),
    Typeahead = require('react-typeahead').Typeahead
import { Table, Glyphicon } from 'react-bootstrap'

function getProducts() { 
  return { 
    products : Services.get_products().map( (p,i) => {
      return {
        id:p.id, 
        title:p.title
      }
    }),
    rows: []
  } 
}

// function getTotals() {
//   let totals = 0
//   $('input[id^="tot-"]').map((i,v)=>{ totals += Number(v.value) })
//   return totals
// }

Services.getProducts()

module.exports = React.createClass({
  mixins: [Mixins(getProducts)],
  getInitialState: function() { 
    return {
      key: 0,
      totals: 0
    }
  },
  displayOption: function(option) { return option.title },
  optionSelected: function(i,option) {
    let rows = Assign([],this.state.rows)
    rows[i].id = option.id
    rows[i].title = option.title
    this.setState({rows:rows})
  },
  titleChange: function(id,e) {
    let rows = Assign([],this.state.rows)
    rows.map((r,i)=>{ if(r.id==id) rows[i].title=e.target.value })
    this.setState({rows:rows})
  },
  multiply: function(id,f,e) {
    let rows = Assign([],this.state.rows),
        val = e.target.value
    rows.map((r,n)=>{
      if(r.id==id) {
        f == 'qty' ? (r.quantity = val) : (r.price = val)
        let tot = ( Number(r.quantity) * Number(r.price) ).toFixed(2)
        rows[n] = {id:r.id,title:r.title,quantity:r.quantity,price:r.price,total:tot}
      }
    })
    this.setState({rows:rows})
  },
  //setTotal: function() { this.setState({total:getTotals()}) },
  remove: function(i) {
    let rows = Assign([],this.state.rows)
    rows.splice(i,1)
    this.setState({rows:rows})//, function() {
      //this.setState({total:getTotals()})
    //}//.bind(this))
  },
  addRow: function() {
    let k = this.state.key,
        rows = Assign([],this.state.rows)
    rows[k] = {id:'new-'+k,title:'',quantity:'',price:'', total:''}
    this.setState({key:++k,rows:rows})
  },
  loadRows: function(entity) {
    let k = this.state.key,
        rows = Assign([],this.state.rows),
        e = entity == undefined ? {id:['new-'+k],title:[''],quantity:[''],price:['']} : entity
    e.id.map((v,i)=>{
      rows[k++] = {id:e.id[i],title:e.title[i],quantity:e.quantity[i],price:e.price[i]}
    })
    this.setState({key:k,rows:rows})
  },
  componentWillMount: function() {
    this.props.data !== undefined ? this.loadRows(this.props.data.products) : this.addRow()
  },
  componentDidMount: function() {
    console.log('did mount')
    let totals = 0
    this.state.rows.map((r,i)=>{
      totals+=r.total
      if(this.state.rows.length==i+1)
        this.setState({totals:totals})
    })
  },
  render: function() {
    let columns = ['', 'الاسم', 'العدد', 'الثمن', 'المجموع'],
        classes = "table-ticket table-bordered table-condensed table-hover"
    return (
      <form className="form-horizontal">
        <h5>المنتوجات :</h5>
        <Table className={classes}>
          <thead><tr>{columns.map((c,i)=>{return<th key={i}>{c}</th>})}</tr></thead>
          <tbody id="sale-items">
            {this.state.rows.map( (r,n) => { let i = r.id
              return (
                <tr key={n}>
                  <td><Glyphicon onClick={this.remove.bind(null,n)} glyph="remove-circle" className="btn-ticket del"/></td>
                  <td>
                    <input type="hidden" name="products[id][]" value={r.id} />
                    <input type="text" name="products[text][]" value={r.title} onChange={this.titleChange.bind(null,r.id)}/>
                    
                  </td>
                  <td><input type="number" name="products[quantity][]" id={'qty-'+i} onChange={this.multiply.bind(null,i,'qty')} value={r.quantity} min="0"/></td>
                  <td><input type="number" name="products[price][]" id={'prx-'+i} onChange={this.multiply.bind(null,i,'prx')} step="0.01" value={r.price} min="0"/></td>
                  <td><input type="number" value={r.total} disabled/></td>
                </tr>
              )
            })}
          </tbody>
          <tfoot>
            <tr><td className="w-50"><Glyphicon onClick={this.addRow} glyph="circle-arrow-left" className="btn-ticket add"/></td>
                <td className="ticket-txt"></td><td className="ticket-num"></td><td className="ticket-num"></td><td className="ticket-tot"><input type="number" value={this.state.totals} disabled/></td></tr>
          </tfoot>
        </Table>
      </form>
    )
  }
})
