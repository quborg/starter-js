'use strict'

let React = require('react'),
    Add = require('./AddSale'),
    DateFilter = require('./DateFilter')
import { ButtonToolbar, Col } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <ButtonToolbar className="tool-bar">
        <Col xs={15} md={10}><Add/></Col>
        <Col xs={9} md={2}><DateFilter/></Col>
      </ButtonToolbar>
    )
  }
})
