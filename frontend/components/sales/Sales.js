'use strict'

var $ = require('jquery'),
    React = require('react'),
    ToolBar = require('./ToolBar'),
    SaleFields = require('./SaleFields'),
    Site = require('../../site/settings'),
    Mixins = require('../../mixins/Mixins'),
    Modal = require('../../templates/Modal'),
    Table = require('../../templates/Table'),
    // Helper = require('../../helpers/Helper'),
    Services = require('../../services/Sales')
import { Grid, Row, Button } from 'react-bootstrap'

function getSales() {
  let sales = Services.get_sales().map((sale,s)=>{
        let unities = 0, total = 0, p = sale.products
        p.quantity.map((qty,i)=>{ unities += Number(qty) })
        p.price.map((prx,i)=>{ total += Number(p.price[i]) * Number(p.quantity[i]) })
        return {
          id: sale.id,
          time: sale.time,
          unities: unities,
          total: total,
          entity: sale
        }
      })
  return { sales: sales }
}

Services.getSales()

module.exports = React.createClass({
  mixins: [Mixins(getSales)],
  getInitialState: function() { 
    return { 
      removeModal: false,
      editModal: false,
      sale: []
    }
  },
  openEdit: function(p) { this.setState({editModal: true, sale: p}) },
  closeEdit: function() { this.setState({editModal: false}) },
  edit: function() { 
    console.log('edit sale and save')
    // if(Helper.ValidateSale('edit-sale')) {
    //   let form = $('#edit-sale form').serialize()
    //   Services.updateProduct(this.state.sale.id,form)
    //   this.closeEdit()
    // }
  },
  openRemove: function(p) { this.setState({removeModal: true, sale: p}) },
  closeRemove: function() { this.setState({removeModal: false}) },
  remove: function() {
    Services.removeSale(this.state.sale.id)
    this.closeRemove()
  },
  render: function() {
    let s = this.state.sale,
        columns = Site.Views.Sales.thead,
        classes = "table-bordered table-condensed table-hover",
        titleR = Site.Views.Sales.removeModal.title,
        submitR = (<Button onClick={this.remove} bsStyle="danger">نعم</Button>),
        cancelR = (<Button onClick={this.closeRemove} bsStyle="primary">لا</Button>),
        titleE = Site.Views.Sales.editModal.title,
        submitE = (<Button onClick={this.edit} bsStyle="info">حفظ</Button>),
        cancelE = (<Button onClick={this.closeEdit}>لا</Button>),
        bodyR = (<div><div>التوقيت: {s.time}</div><div>عدد الوحدات: {s.unities}</div><div>المجموع: {s.total}</div></div>),
        bodyE = <SaleFields data={this.state.sale.entity}/>
    return (
      <Grid fluid>
        <Row className="show-grid sp-b-10" fluid><ToolBar/></Row>
        <Row className="show-grid" fluid>
          <Table className={classes} thead={columns} tbody={this.state.sales} edit={this.openEdit} remove={this.openRemove}/>
        </Row>
        <Modal show={this.state.removeModal} onHide={this.closeRemove} title={titleR} submit={submitR} cancel={cancelR} body={bodyR}/>
        <Modal id="edit-sale" show={this.state.editModal} onHide={this.closeEdit} title={titleE} submit={submitE} cancel={cancelE} body={bodyE}/>
      </Grid>
    )
  }
})
