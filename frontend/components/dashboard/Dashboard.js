'use strict'

import React from 'react';

class Dashboard extends React.Component {

  constructor () {
    super ();
    this.state = {
      blocks: [
        { "name": "Sales Revision" }
      ]
    }
  }

  render () {
    return (
      <div className="row">
        {this.state.blocks.map((bloc)=>{return <div key={bloc.name} item={bloc}>{bloc.name}</div>})}
      </div>
    )
  }

}

export default Dashboard;
