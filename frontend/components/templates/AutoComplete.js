'use strict'

var React = require('react')
import { Table, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({
  getInitialState: function() { 
    return {
      filter: this.props.filter,
      data: []
    }
  },
  onChange: function( option ) {
    console.log(option)
    console.log(option.target.value)
    // let data = this.props.data.map((p,i)=>{
    //   if(p.title) return p.title
    // })
// if ( inputString.indexOf(findme) > -1 ) {
//     alert( "found it" );
// } else {
//     alert( "not found" );
// }
  },
  onClick: function( option ) {
    console.log(option)
  },
  render: function() {
    let a = this.props
    return (
      <div className="autocomplete">
        <input type="hidden" name={a.name} value={a.value}/>
        <input type="text" value={a.value} ref={'test-'+a.key} onChange={this.onChange}/>
        <ul className="autocomplete-selector">
          {this.state.data.map( (r,i) => {
            return (
              <li>
                <a href="javascript:void 0;" onClick={this.onClick(r)} className="typeahead-option">{r.title}</a>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
})
