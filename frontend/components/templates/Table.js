'use strict'

var React = require('react'),
    BtnGly = require('./BtnGly')

import { Table } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <Table className={this.props.className}>
        <thead>
          <tr>
            <th></th>
            {this.props.thead.map((c,i)=>{if(!c.hidden)return<th key={i}>{c.title}</th>})}
          </tr>
        </thead>
        <tbody>
          {this.props.tbody.map((r,i)=>{return(
            <tr key={i} id={r.id}>
              <td className="td-op">
                <BtnGly Style="warning" Click={this.props.edit.bind(null,r)} Gly="pencil" Size="xsmall" Class="btn-op"/>
                <BtnGly Style="danger" Click={this.props.remove.bind(null,r)} Gly="remove" Size="xsmall" Class="btn-op"/>
              </td>
              {this.props.thead.map((c,i)=>{if(!c.hidden)return<td key={i}>{c.name=='category'?r[c.name].tag:r[c.name]}</td>})}
            </tr>
          )})}
        </tbody>
      </Table>
    )
  }
})
