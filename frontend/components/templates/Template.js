'use strict';

import React from 'react';
import Menu from './Menu';
import { Grid } from 'react-bootstrap';
import LightTheme from 'material-ui/lib/styles/baseThemes/lightBaseTheme';
import MyMuiTheme from '../../site/MyMuiTheme';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator';

const muiTheme = ThemeManager.getMuiTheme(LightTheme,MyMuiTheme);

console.log(muiTheme)
// console.log(LightTheme)

@ThemeDecorator(muiTheme)
class Template extends React.Component {

  render() {
    return (
      <Grid fluid /*style={{direction:'rtl'}}*/>
        <Menu/>
        <div className="container-fluid">
          {this.props.children}
        </div>
      </Grid>
    )
  }

}

export default Template;
