'use strict'

import React from 'react';
import AppBar from 'material-ui/lib/app-bar';
import Settings from '../../site/settings';
import TextField from 'material-ui/lib/text-field';
// import { Navbar, NavbarBrand, Nav, Glyphicon } from 'react-bootstrap'
// import { IndexLink } from 'react-router'

  // , Link = require('react-router').Link

class Menu extends React.Component {

  static contextTypes = {
    muiTheme: React.PropTypes.object
  }

  render () {
    console.log(this)
    return (
      <div>
        <AppBar title="Title" iconClassNameRight="muidocs-icon-navigation-expand-more" />
        <TextField hintText="Hint Text"/>
      </div>
    )
  }
}

// Menu.contextTypes = { muiTheme: React.PropTypes.object };

export default Menu;
// module.exports = React.createClass({
//   render: function() {
//     return (

//       {
//         <Navbar fixedTop fluid>
//               <NavbarBrand>
//                 <IndexLink to="/" activeClassName="active"><Glyphicon glyph="stats"/></IndexLink>
//               </NavbarBrand>
//               <Nav>
//                 {Settings.mainMenu.map((l,k)=>(<li key={k}><Link to={l.to} activeClassName="active">{l.title}</Link></li>))}
//               </Nav>
//             </Navbar>
//       }
//     )
//   }
// })
