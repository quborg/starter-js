'use strict'

import he from 'he'
import { Row, Col } from 'react-bootstrap'

var MM    = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

module.exports = {

  /*
  ** @N : number of chars
  ** @word : trim text by words
  ** @clean : remove html tags
  */
  shorten(text, N, word, clean) {
    N = Number(N)
    if (clean) text = text.replace(/(<([^>]+)>)/ig,"")
    text = text.substr(0, N)
    if (word) text = text.substr(0, Math.min(N, text.lastIndexOf(" ")))
    return text
  },

  /* Loader Animation */
  loading() {
    return (
      <Row><Col xs={12} className="text-center">
        <img src="/images/icons/loader.gif" style={style}/>
      </Col></Row>
    )
  }

}
