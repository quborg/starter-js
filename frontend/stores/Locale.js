'use strict'

var CHANGE_EVENT = 'change'
  , assign = require('object-assign')
  , Posts = require('../services/Posts.js')
  , EN = require('../services/locales/EN.js')
  , TR = require('../services/locales/TR.js')
  , Constants = require('../constants/Constants')
  , EventEmitter = require('events').EventEmitter
  , Dispatcher = require('../dispatchers/Dispatcher')
  , Locale = TR
  , Lang = 'tr'
  ;

function setLocaleSetting(lang) {
  Lang = lang;
  Posts.loadStickyPosts(lang)
  Locale = lang == 'tr' ? TR : EN
}

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
  getLocale: function() {
    return Locale;
  },
  getLang: function() {
    return Lang;
  },
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action
    switch (action.actionType) { 
      case Constants.LANGUAGE: setLocaleSetting(action.item); break;
      case Constants.COMMENTS: Posts.setComments(action.comments); break;
      case Constants.BLOG_POSTS: Posts.setPosts(action.posts); break;
      case Constants.STICKY_POSTS: Posts.setStickyPosts(action.posts); break;
    }
    AppStore.emitChange();
    return true;
  })
})

AppStore._maxListeners = 100;

module.exports = AppStore;
