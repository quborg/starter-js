'use strict'

var CHANGE_EVENT = 'change'
  , assign = require('object-assign')
  , Constants = require('../constants/Constants')
  , EventEmitter = require('events').EventEmitter
  , Dispatcher = require('../dispatchers/Dispatcher')
  , User = {}
  , Users = []

var ChatsStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
  getUser: function() {
    return User;
  },
  setUser: function(user) {
    console.log('setUser :',user)
    User = user;
  },
  getUsers: function() {
    return Users;
  },
  setUsers: function(users) {
    console.log('setUsers :',users)
    Users = users;
  },
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action
    switch (action.actionType) { 
      case Constants.USER: ChatsStore.setUser(action.user); break;
      case Constants.USERS: ChatsStore.setUsers(action.users); break;
    }
    ChatsStore.emitChange();
    return true;
  })
})

module.exports = ChatsStore;
