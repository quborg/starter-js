# STARTER-KIT
# Express REST API && ReactJS UI

## 1. Environment

```
npm i express -g
npm i gulp -g
```

## 2. Compile and Transform JSX

```
$ gulp
```
###Note: Currently, gulp is on development mode
## 3. Run the server:
```
$ npm start [development|production]
```
