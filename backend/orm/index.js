'use strict'

var Waterline   = require('waterline')
  , Orm         = new Waterline()
  , Connection  = require('../configs/Connection.js')
  , Product     = require('../api/models/Product.js')

Orm.loadCollection(Product)

module.exports = (App) => {
  Orm.initialize(Connection, function(err, models) {
    if(err) throw err
    App.models = models.collections
    App.connections = models.connections
  })
}
