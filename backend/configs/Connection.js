var Disk = require('sails-disk')

module.exports = {

  adapters: {
    disk: Disk
  },

  connections: {
    productsDb: {
      adapter: 'disk',
      filePath: process.env.DB_PATH,
      fileName: 'products.db'
    }
  },

  defaults: {
    migrate: 'alter',
    schema: true,
    autoPK: false
  }

}
