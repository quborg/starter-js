'use strict'

var config = {
  development: {
    mode: 'development',
    port: 3000
  },
  production: {
    mode: 'production',
    port: 8000
  }
}

module.exports = (App) => {

  let Env   = config[process.env.NODE_ENV]
  App.mode  = Env.mode
  App.port  = Env.port

}
