'use strict'

module.exports = (App, mode) => {

  process.env.DB_PATH   = '/var/www/clients/starter/database/'
  process.env.NODE_ENV  = process.argv[2] || mode

  require('./Environment')(App)
  require('../orm')(App)
  require('../api/routes')(App)

}
