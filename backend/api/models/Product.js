var Waterline = require('waterline')

module.exports = Waterline.Collection.extend({

  identity: 'product',

  connection: 'productsDb',

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    title: {
      type: 'string',
      required: true
    },
    summary: {
      type: 'string'
    },
    sale_price: {
      type: 'float',
      defaultsTo: 0
    },
    purchase_price: {
      type: 'float',
      defaultsTo: 0
    },
    quantity: {
      type: 'integer',
      defaultsTo: 0
    },
    image: {
      type: 'string'
    },
    category: {
      type: 'string',
      defaultsTo: 1
    },
    published: {
      type: 'boolean'
    }
  }

})
