'use strict'

var bodyParser  = require('body-parser')
  , Routes      = {
      product: require('./Product')
    }

module.exports = (App) => {

  App
    .use(function(req, res, next) {
      req.models = App.models
      next()
    })

    .use(bodyParser.urlencoded({extended:true}))
    .use(bodyParser.json())

    .use('/api/products', Routes.product)

    .get('/', function(req,res){
      res.render('index');
    })

}
