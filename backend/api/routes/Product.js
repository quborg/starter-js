'use strict'

var Router = require('express').Router()
  , Controllers = {
      Products: require('../controllers/Products')
    }

Router.get('/', Controllers.Products.getAll)
      .get('/:id', Controllers.Products.findOne)
      .post('/', Controllers.Products.create)
      .put('/:id', Controllers.Products.update)
      .delete('/:id', Controllers.Products.destroy)

module.exports = Router
