'use strinct'

var _ = require('lodash')

module.exports = {

  getAll: function(req, res, next) {
    req.models.product.find(req.query, function(err, products){
      if (err) return res.json({error: err}, 500);
      res.status(200).json(products);
    })
  },

  findOne: function(req, res, next) {
    req.models.product.findOne(req.params.id, function(err, product) {
      if (product === undefined) return res.notFound();
      if (err) return next(err);
      res.status(200).json(product);
    });
  },

  create: function(req, res, next) {
    req.models.product.create(req.body, function(err, product) {
      if (err) return next(err);
      res.status(201).json(product);
    });
  },

  update: function(req, res, next) {
    var id       = req.params.id
      , criteria = _.merge({}, req.params, req.body);
    if (!id) return res.badRequest('No id provided.');
    req.models.product.update(id, criteria, function(err, product) {
      if (product === undefined) return res.notFound();
      if (err) return next(err);
      res.json(product);
    });
  },

  destroy: function(req, res, next) {
    var id = req.param('id');
    if (!id) return res.badRequest('No id provided.');
    req.models.product.findOne(id, function(err, result) {
      if (err) return res.serverError(err);
      if (!result) return res.notFound();
      req.models.product.destroy(id, function(err) {
        if (err) return next(err);
        return res.json(result);
      });
    });
  }

}
